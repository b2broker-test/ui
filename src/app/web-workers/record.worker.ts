/// <reference lib="webworker" />
import { deserialize } from 'class-transformer';

import { Record } from '../models/record';
import { environment } from 'src/environments/environment';
import { WebWorkerParams } from '../models/web-worker-params';
import { interval, Subscription } from 'rxjs';


let webWorkerParams: WebWorkerParams = {
  recordsLimit: environment.defaultRecordLimit,
  idFilter: new Set<string>(),
  updateIntervalMs: environment.defaultUpdateIntervalMs,
  reconnectIntervalMs: environment.defaultRecconnectMs
};
let result: Record[] = [];
let filteredResult: Record[] = [];
let recconnectIntervalSubscription: Subscription | null = null;
let updateIntervalSubscription: Subscription | null = null;
let connection: WebSocket | null;

function addDataToResult(): void {
  if (result.length > 0) {
    let tmp: Record[] = [];
    result = result.slice(0, webWorkerParams.recordsLimit - filteredResult.length);
    tmp = tmp.concat(filteredResult, result);
    postMessage(tmp);
  }
}

/**
 * @param params WebWorkerParams
 */
function onopen(params: WebWorkerParams): void {
  if (updateIntervalSubscription === null) {
    updateIntervalSubscription = interval(params.updateIntervalMs).subscribe(() => {
      addDataToResult();
    });
  }
  if (recconnectIntervalSubscription !== null) {
    recconnectIntervalSubscription.unsubscribe();
    recconnectIntervalSubscription = null;
  }
}

/**
 * @param event MessageEvent<string>
 */
function onmessage(event: MessageEvent<string>): void {
  const record: Record = deserialize(Record, event.data);
  if (webWorkerParams.idFilter && webWorkerParams.idFilter.size > 0) {
    if (webWorkerParams.idFilter.has(record.id)) {
      filteredResult.unshift(record);
    } else {
      result.unshift(record);
    }
  } else {
    result.unshift(record);
  }
}

/**
 * @param params WebWorkerParams
 */
function onclose(params: WebWorkerParams): void {
  if (recconnectIntervalSubscription === null) {
    recconnectIntervalSubscription = interval(params.reconnectIntervalMs).subscribe(() => {
      connectToWebSocket(params);
    });
  }
  if (updateIntervalSubscription !== null) {
    updateIntervalSubscription.unsubscribe();
    updateIntervalSubscription = null;
  }
}

/**
 * @param params WebWorkerParams
 */
function onerror(params: WebWorkerParams): void {
  if (recconnectIntervalSubscription === null) {
    recconnectIntervalSubscription = interval(params.reconnectIntervalMs).subscribe(() => {
      connectToWebSocket(params);
    });
  }
  if (updateIntervalSubscription !== null) {
    updateIntervalSubscription.unsubscribe();
    updateIntervalSubscription = null;
  }
}

/**
 * @param params WebWorkerParams
 */
function connectToWebSocket(params: WebWorkerParams): void {
  if (filteredResult.length > 0) {
    filteredResult = [];
  }
  if (result.length > 0) {
    result = [];
    postMessage(result);
  }
  connection = new WebSocket(environment.socketUrl, 'echo-protocol');
  connection.onopen = () => onopen(params);
  connection.onmessage = (event: MessageEvent<string>) => onmessage(event);
  connection.onerror = () => onerror(params);
  connection.onclose = () => onclose(params);
  if (recconnectIntervalSubscription !== null) {
    recconnectIntervalSubscription.unsubscribe();
    recconnectIntervalSubscription = null;
  }
}

connectToWebSocket(webWorkerParams);

addEventListener('message', (message: MessageEvent<WebWorkerParams>) => {
  if (webWorkerParams.updateIntervalMs !== message.data.updateIntervalMs && updateIntervalSubscription) {
    updateIntervalSubscription.unsubscribe();
    updateIntervalSubscription = interval(webWorkerParams.updateIntervalMs).subscribe(() => {
      addDataToResult();
    });
  }
  webWorkerParams = { ...webWorkerParams, ...message.data };
});
