import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Record } from '../models/record';
import { WebWorkerParams } from '../models/web-worker-params';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private worker: Worker | null = null;
  private readonly webSocketDataSubject: Subject<Record[]> = new Subject();

  constructor() {
    if (typeof Worker !== 'undefined') {
      // Create a new
      this.worker = new Worker('../web-workers/record.worker', { type: 'module' });
      this.worker.onmessage = (message: MessageEvent<Record[]>) => {
        this.webSocketDataSubject.next(message.data);
      };
    } else {
      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
    }
  }

  public updateWebWorkerParams(params: WebWorkerParams): void {
    if (this.worker) {
      this.worker.postMessage(params);
    }
  }

  public get webSocketData(): Subject<Record[]> {
    return this.webSocketDataSubject;
  }
}
