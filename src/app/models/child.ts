export interface ChildInterface {
    id: string;
    color: string;
}

export class Child implements ChildInterface {
    public id = '';
    public color = '';

    constructor(id?: string, color?: string) {
        this.id = id || '';
        this.color = color || '';
    }
}

export const ChildTableHeaders: string[] = ['id', 'color'];
