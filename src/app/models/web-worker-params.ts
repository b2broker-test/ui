export interface WebWorkerParams {
    recordsLimit: number;
    idFilter: Set<string>;
    updateIntervalMs: number;
    reconnectIntervalMs: number;
}
