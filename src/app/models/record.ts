import { ChildInterface, Child } from './child';

export interface RecordInterface extends ChildInterface {
    int: number;
    float: number;
    child: Child;
}

export class Record implements RecordInterface {
    public id = '';
    public color = '';
    public int = 0;
    public float = 0.0;

    public child: Child = new Child();
}

export const RecordTableHeaders: string[] = ['id', 'color', 'int', 'float', 'child'];
