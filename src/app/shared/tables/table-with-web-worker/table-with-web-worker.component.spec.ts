import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableWithWebWorkerComponent } from './table-with-web-worker.component';
import { MatTableModule } from '@angular/material/table';
import { Child } from 'src/app/models/child';

describe('TableWithWebWorkerComponent', () => {
  let component: TableWithWebWorkerComponent;
  let fixture: ComponentFixture<TableWithWebWorkerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatTableModule],
      declarations: [TableWithWebWorkerComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableWithWebWorkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#isChildColumn correct', () => {
    const correctData = new Child('1', 'red');
    expect(component.isChildColumn(correctData)).toBe(true);
  });

  it('#isChildColumn incorrect with string', () => {
    const stringData = 'test';
    expect(component.isChildColumn(stringData)).toBe(false);
  });

  it('#isChildColumn incorrect with string', () => {
    const numberData = 2;
    expect(component.isChildColumn(numberData)).toBe(false);
  });
});
