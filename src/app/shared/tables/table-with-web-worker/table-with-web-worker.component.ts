import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Child, ChildTableHeaders } from 'src/app/models/child';
import { Record } from 'src/app/models/record';

@Component({
  selector: 'app-table-with-web-worker',
  templateUrl: './table-with-web-worker.component.html',
  styleUrls: ['./table-with-web-worker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableWithWebWorkerComponent {

  @Input()
  public dataSource: Observable<Record[]> | Child[] = [];
  @Input()
  public gridHeader: string[] = [];
  public readonly childGridHeader: string[] = ChildTableHeaders;

  constructor() {
  }

  public isChildColumn(data: string | number | Child): boolean {
    if (typeof data === 'object') {
      const tmp = new Child(data.id, data.color);
      if (tmp instanceof Child) {
        return true;
      }
      return false;
    }
    return false;
  }
}
