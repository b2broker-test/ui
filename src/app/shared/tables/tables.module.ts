import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableWithWebWorkerComponent } from './table-with-web-worker/table-with-web-worker.component';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TableWithWebWorkerControlsComponent } from './table-with-web-worker-controls/table-with-web-worker-controls.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    TableWithWebWorkerComponent,
    TableWithWebWorkerControlsComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports: [
    TableWithWebWorkerComponent,
    TableWithWebWorkerControlsComponent
  ]
})
export class TablesModule { }
