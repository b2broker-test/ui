import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { debounceTime } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';


@Component({
  selector: 'app-table-with-web-worker-controls',
  templateUrl: './table-with-web-worker-controls.component.html',
  styleUrls: ['./table-with-web-worker-controls.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableWithWebWorkerControlsComponent implements OnInit {

  public formGroup: FormGroup = new FormGroup({
    updateIntervalMs: new FormControl(environment.defaultUpdateIntervalMs, [Validators.required, Validators.pattern('^[0-9]+$')]),
    recordsLimit: new FormControl(environment.defaultRecordLimit, [Validators.required, Validators.pattern('^[0-9]+$')]),
    idFilter: new FormControl() // TODO - add validator
  });

  constructor(private apiService: ApiService) { }

  public ngOnInit(): void {
    this.formGroup.valueChanges.pipe(
      debounceTime(200)
    ).subscribe((values) => {
      if (values.idFilter) {
        values.idFilter = new Set((values.idFilter as string).split(','));
      } else {
        values.idFilter = null;
      }
      values.updateIntervalMs = +values.updateIntervalMs;
      values.recordsLimit = +values.recordsLimit;
      this.apiService.updateWebWorkerParams(values);
    });
  }

}
