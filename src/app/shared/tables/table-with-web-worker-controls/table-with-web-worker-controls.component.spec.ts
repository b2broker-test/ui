import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableWithWebWorkerControlsComponent } from './table-with-web-worker-controls.component';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('TableWithWebWorkerControlsComponent', () => {
  let component: TableWithWebWorkerControlsComponent;
  let fixture: ComponentFixture<TableWithWebWorkerControlsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatInputModule
      ],
      declarations: [ TableWithWebWorkerControlsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableWithWebWorkerControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
