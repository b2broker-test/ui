import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Record, RecordTableHeaders } from '../models/record';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  public readonly dataSource: Observable<Record[]> = this.apiService.webSocketData;
  public readonly recordTableHeaders: string[] = RecordTableHeaders;

  constructor(private apiService: ApiService) { }

}
