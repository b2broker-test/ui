export const environment = {
  production: true,
  socketUrl: 'ws://localhost:8888',
  defaultUpdateIntervalMs: 1000,
  defaultRecordLimit: 10,
  defaultRecconnectMs: 1000
};
