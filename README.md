# BrokerUi

# About project

This project is a resolution of [test task](https://gitlab.com/b2broker-test/taskdescription)

# Prerequisites

- Install NodeJS v12.22.4 with NPM (Node Package Manager)
- Install depencies:
```npm ci```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Run WebSocket emulator

- Clone repository https://gitlab.com/b2broker-test/emulator\
- Go to project directory
- Run command:
```node emulator.js```

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).